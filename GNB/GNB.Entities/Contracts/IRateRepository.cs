﻿using Gnb.Library.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gnb.Library.Contracts
{
    public interface IRateRepository
    {
        Task<List<Rate>> GetAllRates();
    }
}
