﻿using System.Text.Json.Serialization;

namespace Entities
{
    public class Rate
    {
        public string From { get; set; }
        public string To { get; set; }


        [JsonPropertyName("rate")]
        public string rate { get; set; }
    }
}
