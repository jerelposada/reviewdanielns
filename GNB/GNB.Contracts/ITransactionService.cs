﻿
using Gnb.Contracts.Service.Library.Dto;
using Gnb.Library.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gnb.Contracts.Service.Library
{
    public interface ITransactionService
    {
        Task<List<Transaction>> GetTransactions();
        Task<ResponseTransactionDTO> SearchTransaction(string sku);
    }
}
