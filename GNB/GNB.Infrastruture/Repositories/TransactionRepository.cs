﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Gnb.Infrastructure.Repositories.fileService;
using Gnb.Library.Contracts;
using Gnb.Library.Entities;
using Microsoft.Extensions.Logging;

namespace Gnb.Infrastructure.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly ILogger<TransactionRepository> logger;
        static readonly HttpClient httpClient = new();
        private const string URI_REQUEST = "http://quiet-stone-2094.herokuapp.com/transactions.json";
        private readonly string DIRECTORY = Environment.CurrentDirectory + @"\trasanction.txt";
        public TransactionRepository(ILogger<TransactionRepository> logger)
        {
            this.logger = logger;
        }
        /// <summary>
        /// get all transaction
        /// </summary>
        /// <returns> List of trasaction </returns>
        public async Task<List<Transaction>> GetAllTransactions()
        {
            List<Transaction> transactions = new();
            try
            {
                var response = await httpClient.GetAsync(URI_REQUEST);

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    transactions = JsonSerializer.Deserialize<List<Transaction>>(
                    responseBody, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, WriteIndented = true });
                    FileManagementService.LocalDataStore(transactions, DIRECTORY);
                }
                else
                {
                    transactions = (List<Transaction>)FileManagementService.LoadData<Transaction>(DIRECTORY);
                }

            }
            catch (Exception ex)
            {
                logger.LogError($"class TransactionRepository ${ex.Message}");
            }
           
            return transactions;
        }



    }
}
