﻿
using Gnb.Infrastructure.Repositories.fileService;
using Gnb.Library.Contracts;
using Gnb.Library.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;


namespace Gnb.Infrastructure.Repositories
{
    public  class RateRepository: IRateRepository
    {
        private readonly ILogger<RateRepository> logger;
   
        private  const string URI_REQUEST = "http://quiet-stone-2094.herokuapp.com/rates.json";
        private readonly string DIRECTORY = Environment.CurrentDirectory + @"\Rates.txt";
        static readonly HttpClient httpClient = new HttpClient();
        public RateRepository(ILogger<RateRepository> logger)
        {
            this.logger = logger;

        }  

        /// <summary>
        ///  Get all Rates
        /// </summary>
        /// <returns> List Rates</returns>
        public async Task<List<Rate>> GetAllRates()
        {
            List<Rate> listRates = new();
            try
            {
                var response = await httpClient.GetAsync(URI_REQUEST);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    listRates = JsonSerializer.Deserialize<List<Rate>>(responseBody,
                    new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, WriteIndented = true });
                    FileManagementService.LocalDataStore(listRates, DIRECTORY);
                }
                else
                {
                    listRates = (List<Rate>)FileManagementService.LoadData<Rate>(DIRECTORY);
                }
           
            }
            catch(Exception ex)
            {
                logger.LogError($" class RateRepository ${ex.Message}");
            }
  
            return listRates;
        }
    }
}
