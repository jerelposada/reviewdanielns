using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gnb.Contracts.Service.Library;
using Gnb.Contracts.Service.Library.Dto;
using Gnb.Library.Contracts;
using Gnb.Library.Entities;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Gnb.Impl.ServiceLibrary.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IRateRepository rateRepository;
        private readonly ITransactionRepository transactionRepository;
        private readonly IMemoryCache memoryCache;
        private readonly ILogger<TransactionService> logger;

        public TransactionService(IMemoryCache memoryCache, IRateRepository rateRepository, ITransactionRepository transactionRepository, ILogger<TransactionService> logger)
        {
           this.memoryCache = memoryCache;
         this.transactionRepository = transactionRepository;
         this.rateRepository = rateRepository;
            this.logger = logger;
        }

        /// <summary>
        /// Get Trasanction
        /// </summary>
        /// <returns> <List<Transaction>> </returns>
        public async Task<List<Transaction>> GetTransactions()
        {

            memoryCache.Set("AllTransactions", await transactionRepository.GetAllTransactions());
            return await memoryCache.GetOrCreateAsync("AllTransactions",
                cacheListFlight =>
                {
                    return transactionRepository.GetAllTransactions();
                });
        }

        /// <summary> 
        ///  Search transaction
        /// </summary>
        /// <param name="sku"></param>
        /// <returns>Array Object[]</returns>
        public async Task<ResponseTransactionDTO> SearchTransaction(string sku)
        {
            List<Transaction> responseTransactions = new();
            ResponseTransactionDTO response = new();
            try
            {
                List<Transaction> transactions = await GetTransactions();
                responseTransactions = transactions.FindAll(transaction => transaction.Sku.ToUpper() == sku.ToUpper()).ToList();
                response = await ChangeCurrencyTransaction(responseTransactions);

            }
            catch (Exception ex)
            {

                logger.LogError($"class TransactionService ${ex.Message}");

            }
            return response;


        }

        /// <summary>
        /// Change Currency Transaction
        /// </summary>
        /// <param name="transactions"></param>
        /// <returns> array of object</returns>
        internal async Task<ResponseTransactionDTO> ChangeCurrencyTransaction(List<Transaction> transactions)
        {
            string tariff = "";
            decimal totalAmount = 0;
            List<Rate> rates = await rateRepository.GetAllRates();
            foreach (Transaction transaction in transactions)
            {
                if (transaction.Currency.Equals("EUR"))
                {
                    totalAmount = Convert.ToDecimal(transaction.Amount) + totalAmount;
                }
                else
                {
                    tariff = rates.Where(r => r.From == transaction.Currency && r.To == "EUR").Select(r => r.rate).FirstOrDefault();
                    if (tariff == null)
                    {
                        Transaction transactionResponse = ConversionRatesIntermediate(rates, transaction);
                        transaction.Currency = transactionResponse.Currency;
                        transaction.Amount = transactionResponse.Amount;
                        tariff = rates.Where(r => r.From == transaction.Currency && r.To == "EUR").Select(r => r.rate).FirstOrDefault();
                    }
                    transaction.Amount = ConversionCalculator(tariff, transaction.Amount).ToString();
                    totalAmount = Convert.ToDecimal(transaction.Amount) + totalAmount;
                    transaction.Currency = "EUR";
                }
            }

            return new ResponseTransactionDTO
            {   
                Transactions = transactions,
                TotalAmount = totalAmount
            };
        }



        /// <summary>
        ///  calculator convertion
        /// </summary>
        /// <param name="tariff"></param>
        /// <param name="amount"></param>
        /// <returns> calculated value</returns>
        internal decimal ConversionCalculator(string tariff, string amount)
        {
           if(tariff != null)
            {
                return decimal.Round((Convert.ToDecimal(tariff.Replace(".", ",")) * Convert.ToDecimal(amount.Replace(".", ","))), 2);
            }
            else
            {
                logger.LogError(" the class TransactionService: NoT Exis Tariff");
            }
            return 0.0M;
        }

        /// <summary>
        /// converts the currency of the 
        /// transactions into an intermediate currency
        /// </summary>
        /// <param name="rates"></param>
        /// <param name="trasanction"></param>
        /// <returns> Trasaction</returns>
        internal  Transaction ConversionRatesIntermediate(List<Rate> rates, Transaction transaction)
        {
            List<Rate> rateFilter = rates.FindAll( r => r.From == transaction.Currency && r.To != "EUR").ToList();
            List<Rate> listRateToFilter = rates.FindAll( r => r.To == "EUR").ToList();
            foreach(Rate rate in rateFilter)
            {
                List<Rate> listRates = listRateToFilter.Where(r => rate.To == r.From).ToList();
                if (listRates.Count > 0)
                {
                    transaction.Amount = ConversionCalculator(rate.rate, transaction.Amount).ToString();
                    transaction.Currency = rate.To;
                    return transaction;
                }
            }

            return transaction;
        }
      
    }

}
