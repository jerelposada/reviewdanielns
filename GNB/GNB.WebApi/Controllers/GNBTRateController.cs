﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Gnb.Library.Entities;
using Gnb.Contracts.Service.Library;

namespace Vueling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GnbRateController : ControllerBase
    {
        private readonly IRateService _rateService;
        public GnbRateController(IRateService rateService)
        {
            _rateService = rateService;
        }

        [HttpGet("rates")]
        public async Task<List<Rate>> GetRate()
        {
            List<Rate> Rates = await  _rateService.GetRates();
            return Rates;
        }
        
    }
}
