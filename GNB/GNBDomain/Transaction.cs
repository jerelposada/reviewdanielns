﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public class Transaction
    {
        public string Sku { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
    }
}
