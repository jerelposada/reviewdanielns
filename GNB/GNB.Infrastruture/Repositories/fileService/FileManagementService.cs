﻿using Gnb.Library.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Gnb.Infrastructure.Repositories.fileService
{
    public static class FileManagementService
    {



        /// <summary>
        /// store data Local
        /// </summary>
        /// <param name="rates"></param>
        public static void LocalDataStore<T>(List<T> listData, string directory)
        {
            try
            {
                string contentData = JsonSerializer.Serialize(listData);
                File.WriteAllText(directory, contentData);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        /// <summary>
        ///  Load data local
        /// </summary>
        /// <returns> List<Trasanction> </returns>
        public static IEnumerable<T> LoadData<T>(string directory)
        {
           IEnumerable<T> responseData =  new List<T>();

            if (!File.Exists(directory))
            {
                return responseData;
            }

            string rateString = File.ReadAllText(directory);
            responseData = JsonSerializer.Deserialize<List<T>>(rateString,
                    new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, WriteIndented = true });

            return responseData;
        }
    }
}
