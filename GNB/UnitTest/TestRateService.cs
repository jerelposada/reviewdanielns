using Gnb.Impl.ServiceLibrary.Services;
using Gnb.Infrastructure.Repositories;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gnb.Service.UnitTest
{
    [TestClass]
    public class TestRateService
    {

        public IMemoryCache InyectionThememoryCache()
        {

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            return memoryCache;
        }


        /// <summary>
        /// comprobar que el metodo encargado de buscar todas las 
        /// trasacciones lo realice de manera correcta
        /// </summary>
        [TestMethod]
        public void TestGetRate()
        {
            var objTransactionRepository = new TransactionRepository(null);
            var objRateRepository = new RateRepository(null);
            var memoryCache = InyectionThememoryCache();
            var rateService = new RateService(memoryCache, objRateRepository);
            var transaction = rateService.GetRates();
            Assert.IsNotNull(transaction);
        }
    }
}