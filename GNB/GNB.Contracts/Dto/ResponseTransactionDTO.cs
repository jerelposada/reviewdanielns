﻿using Gnb.Library.Entities;
using System;
using System.Collections.Generic;


namespace Gnb.Contracts.Service.Library.Dto
{
    public class ResponseTransactionDTO
    {
        public List<Transaction> Transactions { get; set; }
        public Decimal TotalAmount { get; set; }
    }
}