﻿
using Gnb.Infrastructure.Repositories;
using Gnb.Infrastructure.Repositories.fileService;
using Gnb.Library.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Gnb.Aplication.ServicesContainer
{
    public static class ServicesContainers
    {
       public static IServiceCollection ServiceRegister(this IServiceCollection services)
        {
            
            services.AddSingleton<ITransactionRepository, TransactionRepository>();
            services.AddSingleton<IRateRepository, RateRepository>();
            return services;
        }

    }
}