using Gnb.Impl.ServiceLibrary.Services;
using Gnb.Infrastructure.Repositories;
using Gnb.Library.Entities;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Gnb.Service.UnitTest
{
    [TestClass]
    public class TestTransactionService
    {

        public IMemoryCache InyectionThememoryCache()
        {

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            return memoryCache;
        }


        /// <summary>
        /// 
      //heck that the method in charge of searching all the
        /// transactions do it correctly
        /// </summary>
        [TestMethod]
        public void TestGetTransaction()
        {
            var objTransactionRepository = new TransactionRepository(null);
            var objRateRepository = new RateRepository(null);
            var memoryCache = InyectionThememoryCache();
            var transactionService = new TransactionService(memoryCache, objRateRepository, objTransactionRepository, null);
            var transaction = transactionService.GetTransactions();
            Assert.IsNotNull(transaction);
        }

        /// <summary>
        /// verify if the method in charge of converting 
        /// between currencies performs the conversion properly
        /// </summary>
        [TestMethod]
        public void TestConversionCalculator()
        {
            var objTransactionRepository = new TransactionRepository(null);
            var objRateRepository = new RateRepository(null);
            var memoryCache = InyectionThememoryCache();
            var transactionService = new TransactionService(memoryCache, objRateRepository, objTransactionRepository,null);

            decimal transaction = transactionService.ConversionCalculator("10,0", "0,736");
            Assert.AreEqual(7.36M, transaction);
        }

        /// <summary>
        ///look for transactions where the sku is the same
        /// </summary>
        [TestMethod]
        public void TestSearchTransaction()
        {
            var objTransactionRepository = new TransactionRepository(null);
            var objRateRepository = new RateRepository(null);
            var memoryCache = InyectionThememoryCache();
            var transactionService = new TransactionService(memoryCache, objRateRepository, objTransactionRepository,null);
            var result = transactionService.SearchTransaction("T2006");
            Assert.IsNotNull(result);
        }


        /// <summary>
        /// convert from one currency to another
        /// </summary>
        [TestMethod]
        public  void TestConversionRatesIntermediate()
        {
            var objTransactionRepository = new TransactionRepository(null);
            var objRateRepository = new RateRepository(null);
            var memoryCache = InyectionThememoryCache();
            var transactionService = new TransactionService(memoryCache, objRateRepository, objTransactionRepository,null);
            List<Rate> rates = new List<Rate>();
            Transaction transaction = new Transaction();
            transaction.Sku = "S8976";
            transaction.Amount = "20,1";
            transaction.Currency = "AUD";

            Rate rate = new Rate();
            rate.rate = "0,73";
            rate.To = "CAD";
            rate.From = "EUR";   
            
            Rate rate1 = new Rate();
            rate1.rate = "1,37";
            rate1.To = "EUR";
            rate1.From = "CAD";

            Rate rate2 = new Rate();
            rate2.rate = "1,82";
            rate2.To = "USD";
            rate2.From = "EUR";

            Rate rate3 = new Rate();
            rate3.rate = "1,1";
            rate3.To = "AUD";
            rate3.From = "CAD";
                   
            Rate rate4 = new Rate();
            rate4.rate = "0,91";
            rate4.To = "CAD";
            rate4.From = "AUD";
             
            Rate rate5 = new Rate();
            rate5.rate = "0,55";
            rate5.To = "EUR";
            rate5.From = "USD";

            rates.Add(rate);
            rates.Add(rate1);
            rates.Add(rate2);
            rates.Add(rate3);
            rates.Add(rate4);
            rates.Add(rate5);
            Transaction transactionResponse = new Transaction();
            transactionResponse = transactionService.ConversionRatesIntermediate(rates, transaction);
            Assert.AreEqual("18,29", transactionResponse.Amount);
        }


    }
}
