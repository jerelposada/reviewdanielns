﻿
using Gnb.Contracts.Service.Library;
using Gnb.Library.Contracts;
using Gnb.Library.Entities;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gnb.Impl.ServiceLibrary.Services
{
    public class RateService : IRateService
    { 
        private readonly IMemoryCache memoryCache;
        private readonly IRateRepository rateRepository;

        public RateService(IMemoryCache memoryCache, IRateRepository rateRepository)
        {
            this.memoryCache = memoryCache;
            this.rateRepository = rateRepository;
        }

        /// <summary>
        /// get all rates
        /// </summary>
        /// <returns>List Rates</returns>
        public async Task<List<Rate>> GetRates()
        {
            memoryCache.Set("AllRates", await rateRepository.GetAllRates());

            return await  memoryCache.GetOrCreateAsync("AllRates",
                 cacheListFlight =>
                {
                    return  rateRepository.GetAllRates(); 
                });
        }
    }
}
