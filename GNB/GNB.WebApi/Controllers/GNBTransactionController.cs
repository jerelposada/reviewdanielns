﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Gnb.Contracts.Service.Library;

namespace Vueling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GnbTransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;
        private readonly ILogger<GnbTransactionController> _logger;
        public GnbTransactionController(ITransactionService transactionService, ILogger<GnbTransactionController> logger)
        {
            _transactionService = transactionService;
            _logger = logger;
        }
 
        [HttpGet("transactions")]
        public async Task<IActionResult> GetTransactions()
        {
            var trasanctionResponse = await _transactionService.GetTransactions();

            if(trasanctionResponse == null)
            {
                return NotFound();
            }
            return Ok(trasanctionResponse);
        }

      
        [HttpGet("transactions/{sku}")]
        public  async Task<IActionResult> GetTransaction(string sku)
        {
            _logger.LogInformation(sku);
            var trannsactionSkuResponse = await _transactionService.SearchTransaction(sku);
            if(trannsactionSkuResponse == null)
            {
                return NotFound();
            }
            return Ok(trannsactionSkuResponse);

        }  
        
    }
}