﻿using Gnb.Library.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gnb.Library.Contracts
{
    public interface ITransactionRepository
    {
        Task<List<Transaction>> GetAllTransactions();

    }

}