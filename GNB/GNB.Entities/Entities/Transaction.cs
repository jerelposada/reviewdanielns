﻿using System;

namespace Gnb.Library.Entities
{
    public class Transaction
    {
        public string Sku { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
    }
}
