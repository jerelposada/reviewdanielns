﻿
using Gnb.Contracts.Service.Library;
using Gnb.Impl.ServiceLibrary.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Gnb.Contracts.ServiceContainerBusiness
{
    public static class ServiceContainerBusiness
    {
        public static IServiceCollection ServiceRegisterBusiness(this IServiceCollection services)
        {
            services.AddSingleton<ITransactionService, TransactionService>();
            services.AddSingleton<IRateService, RateService>();

            return services;
        }
    }
}
