﻿using Gnb.Library.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gnb.Contracts.Service.Library
{
    public interface IRateService
    {
        Task<List<Rate>> GetRates();
    }
}
