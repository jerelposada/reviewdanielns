﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gnb.Library.Contracts
{
    public interface IFileManagementRepository
    {

        void LocalDataStore<T>(List<T> listData, string directory);
    }
}
